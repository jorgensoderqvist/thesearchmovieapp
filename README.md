
# theSearchMovieApp

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app) and uses [React](https://facebook.github.io/react/) with [webpack](http://webpack.github.io/), and [babeljs](https://babeljs.io/). Setup to use SASS as the CSS preprocessor.

## Before getting started

**You’ll need to have Node >= 4 on your machine**.
Strongly recommend to use Node >= 6 and npm >= 3 for faster installation speed and better disk usage.

### Get started



```

git clone https://bitbucket.org/jorgensoderqvist/thesearchmovieapp.git
cd thesearchmovieapp
npm intall
npm run start

```


## Running the tests

create-react-app ships with Jest test-framework and I've also added enzyme for better control over the react components.


Run the tests by typing:

```

npm run test

```


## Deployment

When it's time to deploy.You simply run:


```

npm run build

```

## Compatibility

I've used [BrowserStack](https://www.browserstack.com) and it should look good on

* Windows 10 - IE 11, Edge 14, FireFox 52 and Chrome 56.
* Mac with OS El Capitan - Safari 9.1, FireFox 52 and Chrome 56.
* Iphone 6, 6s and 7.
* Samsung Galaxy S7 and Samsung Galaxy S6
* Ipad Air 2 (iOS 9.3) and Ipad Mini 4 (iOS 9.3)
* Samsung Galaxy Tab 4
