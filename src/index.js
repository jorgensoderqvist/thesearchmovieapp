import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './utils/Route';

ReactDOM.render(
    <Routes/>,
  document.getElementById('root')
);
