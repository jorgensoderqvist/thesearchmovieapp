import React from 'react';
import API from './API';


it('should load the toplist', () => {
    return API.getTopMovies().then((data)=>{
        expect(data).toBeDefined();
        expect(data[0].id).toBeDefined();
    });
});
it('should search for movies', () => {
    return API.searchMovie('star wars').then((data)=>{
        expect(data).toBeDefined();
        expect(data[0].title).toContain('Star Wars');
    });
});

it('should get the right Actor', () => {
    return API.getActor('6968').then((data)=>{
        expect(data).toBeDefined();
        expect(data.name).toBe('Hugh Jackman');
    });
});
it('should have a list of movies based on Actor', () => {
    return API.getMoviesByActor('6968').then((data)=>{
        expect(data).toBeDefined();
        expect(data.length).toBeGreaterThan(1);
    });
});

it('should have a list of movies', () => {
    return API.getMovieActors('1669').then((data)=>{
        expect(data).toBeDefined();
        expect(data.length).toBeGreaterThan(1);
    });
});

it('should detail data of a movie', () => {
    return API.getMovieDetail('1669').then((data)=>{
        expect(data).toBeDefined();
        expect(data.title).toBe('The Hunt for Red October');
    });
});



// import axios from 'axios';
// import sinon from 'sinon';
// import API from './API';

// let sandbox;
// let server;
// beforeEach(() => {
//     sandbox = sinon.sandbox.create();
//     server = sandbox.useFakeServer();
// });
// afterEach(() => {
//     server.restore();
//     sandbox.restore();
// });
//
//
// it('should display the data', (done) => {
//     // API.getTopMovies().then((a)=>{
//     //     console.log(a, done);
//     // })
//     // .then(done, done);
//     // setTimeout(() => server.respond([200,
//     //     { 'Content-Type': 'application/json' },
//     // JSON.stringify(['john','doe','pogi'])]), 0);
// });