import React from 'react';
import { shallow } from 'enzyme';
import { Route } from 'react-router';
import Routes from './Route';

import Layout from '../components/layout/Layout';
//pages
import Query from '../components/pages/QueryPage';
import Movie from '../components/pages/MoviePage';
import Actor from '../components/pages/ActorPage';

it('renders correct routes', () => {
    const wrapper = shallow(<Routes />);

    const pathMap = wrapper.find(Route).reduce((pathMap, route) => {
        const routeProps = route.props();
        pathMap[routeProps.path] = routeProps.component;
        return pathMap;
    }, {});

    expect(pathMap['/']).toBe(Layout);
    expect(pathMap['movie/:movieId']).toBe(Movie);
    expect(pathMap['actor/:actorId']).toBe(Actor);

});