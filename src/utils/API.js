/**
 * Created by jorgensoderqvist on 2017-03-04.
 */

import axios from 'axios';

const API_KEY = '9618628fa1ffaa6ea5cbdc6f8f5a44d3';
const API_URL = 'https://api.themoviedb.org/3/';
const LOCALE = "en-US" // sv-SE;

const API = {

    getTopMovies(){
        return axios.get(`${API_URL}movie/popular?page=1&language=${LOCALE}&api_key=${API_KEY}`)
            .then((data)=> {
                return data.data.results
            })
            .catch(function (error) {
                console.log(error);
            });
    },
    searchMovie(value:string){
        return axios.get(`${API_URL}search/movie?include_adult=false&page=1&query=${value}&language=${LOCALE}&api_key=${API_KEY}`)
            .then((data)=> {
                return data.data.results
            })
            .catch(function (error) {
                console.log(error);
            });
    },
    getMovieDetail(movieId){
        return axios.get(`${API_URL}movie/${movieId}?language=${LOCALE}&api_key=${API_KEY}`)
            .then((data)=> {
                return data.data
            })
            .catch(function (error) {
                console.log(error);
            });
    },
    getMovieActors(movieId){
        return axios.get(`${API_URL}movie/${movieId}/credits?api_key=${API_KEY}`)
            .then((data)=> {
                return data.data.cast
            })
            .catch(function (error) {
                console.log(error);
            });
    },
    getActor(personId){
        return axios.get(`${API_URL}person/${personId}?api_key=${API_KEY}&language=${LOCALE}`)
            .then((data)=> {
                return data.data
            })
            .catch(function (error) {
                console.log(error);
            });
    },
    getMoviesByActor(personId){
        return axios.get(`${API_URL}person/${personId}/movie_credits?api_key=${API_KEY}&language=${LOCALE}`)
            .then((data)=> {
                return data.data.cast;
            })
            .catch(function (error) {
                console.log(error);
            });
    }
};
export default API;
