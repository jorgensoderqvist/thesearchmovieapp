import React from 'react';
import {Router, Route, IndexRoute, browserHistory, Redirect} from 'react-router';
import Layout from '../components/layout/Layout';

//pages
import Query from '../components/pages/QueryPage';
import Movie from '../components/pages/MoviePage';
import Actor from '../components/pages/ActorPage';

export default props => (
    <Router history={browserHistory} >
        <Route path="/" component={Layout}>
            <IndexRoute component={Query}/>
            <Route path="movie/:movieId" component={Movie} />
            <Route path="actor/:actorId" component={Actor} />
        </Route>
        <Redirect from="/*" to="/" />
    </Router>
)