import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import RankStar from './RankStar';


let simpleProps = {
    rank:3
}

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<RankStar {...simpleProps}></RankStar>, div);
});

it('Gives two stars of 5', () => {
    const wrapper = shallow(<RankStar {...simpleProps}></RankStar>);
    expect(wrapper.html()).toEqual(
        `<div class="rank-star">★★☆☆☆</div>`
    );
});

it('Gives 0 stars of 5', () => {
    const wrapper = shallow(<RankStar {...simpleProps} rank={0}></RankStar>);
    expect(wrapper.html()).toEqual(
        `<div class="rank-star">☆☆☆☆☆</div>`
    );
});

it('Gives returns empty if rank is undefined', () => {
    const wrapper = shallow(<RankStar {...simpleProps} rank={undefined}></RankStar>);
    expect(wrapper.html()).toEqual(
        `<span></span>`
    );
});