import React, {Component} from 'react';
import './RankStar.scss';

class RankStar extends Component {
    render(){
        if(this.props.rank===undefined){
            return <span></span>;
        }

        let stars=''
        let i=0;
        for(i; i< this.props.rank/2;i++){
            stars+='★';
        }
        for(i; i<5;i++){
            stars+='☆';
        }
        return <div className="rank-star">
            {stars}
        </div>
    }
}

export default RankStar;