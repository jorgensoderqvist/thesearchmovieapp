import React from 'react';
import ReactDOM from 'react-dom';
import {shallow, mount, render} from 'enzyme';
import QueryPage from './QueryPage';
import SearchField from './../search-field/SearchField'

import axios from 'axios';
import MockAdapter from 'axios-mock-adapter'

let wrapper;

const mock = new MockAdapter(axios);


mock.onGet(/.movie\/popular\?./).reply(200, {
    results: [
        {
            "poster_path": "/e1mjopzAS2KNsvpbpahQ1a6SkSn.jpg",
            "adult": false,
            "overview": "From DC Comics comes the Suicide Squad, an antihero team of incarcerated supervillains who act as deniable assets for the United States government, undertaking high-risk black ops missions in exchange for commuted prison sentences.",
            "release_date": "2016-08-03",
            "genre_ids": [
                14,
                28,
                80
            ],
            "id": 297761,
            "original_title": "Suicide Squad",
            "original_language": "en",
            "title": "Suicide Squad",
            "backdrop_path": "/ndlQ2Cuc3cjTL7lTynw6I4boP4S.jpg",
            "popularity": 48.261451,
            "vote_count": 1466,
            "video": false,
            "vote_average": 5.91
        }
    ]
});

mock.onGet(/.search\/movie./).reply(200, {
        results: [{
            "poster_path": "/cezWGskPY5x7GaglTTRN4Fugfb8.jpg",
            "adult": false,
            "overview": "When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!",
            "release_date": "2012-04-25",
            "genre_ids": [
                878,
                28,
                12
            ],
            "id": 24428,
            "original_title": "The Avengers",
            "original_language": "en",
            "title": "The Avengers",
            "backdrop_path": "/hbn46fQaRmlpBuUrEiFqv0GDL6Y.jpg",
            "popularity": 7.353212,
            "vote_count": 8503,
            "video": false,
            "vote_average": 7.33
        }]
    }
);

beforeAll((done) => {
    wrapper = mount(<QueryPage ></QueryPage>);
    setTimeout(() => {
        wrapper.update();
        done();
    }, 1000);
});
afterAll((done) => {
    mock.reset();
});

it('should have the title of Top Movies', ()=> {
    expect(wrapper.state('searchTitle')).toBe('Top Movies');
});

it('should have a list of top movies (one)', ()=> {
    expect(wrapper.state('queryList').length).toBe(1);
});

it('should return a new list of movies (one)', (done)=> {
    wrapper.instance().onSearchChanged('The Avengers');
    setTimeout(() => {
        wrapper.update();
        done();
        expect(wrapper.state('queryList').length).toBe(1);
        expect(wrapper.state('searchTitle')).toBe("Resaults for The Avengers");
    }, 10);

});

