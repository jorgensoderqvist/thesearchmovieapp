import React, {Component} from 'react';
import {browserHistory} from 'react-router';
import SearchField from '../search-field/SearchField'
import MovieList from './../movie-list/MovieList'
import API from '../../utils/API';
import {IMAGE} from '../../config/Constants';

import Logo from '../../images/logo.svg';
import {LoadingManager} from '../loader/Loader';

import './QueryPage.scss';

let query = '';
let queryList = [];
let bgImage = '';

class Query extends Component {

    constructor() {
        super();
        this.state = {queryList: queryList, lastQuery: query, bgImage: bgImage};

        if (queryList.length === 0) {
            API.getTopMovies().then((data)=> {
                this.setState({
                    queryList: data,
                    bgImage: IMAGE.base_url + IMAGE.backdrop_sizes[2] + data[0].backdrop_path,
                    searchTitle: 'Top Movies',
                });

                LoadingManager.hide();
            });
        }
    };

    componentWillMount() {
        
        this.setState({queryList: queryList, lastQuery: query})
    };

    componentWillUnmount() {
        query = this.state.lastQuery;
        queryList = this.state.queryList;
        bgImage = this.state.bgImage;
    };

    onSearchChanged(data) {
        API.searchMovie(data).then((response)=> {
            this.setState({
                queryList: response || [],
                lastQuery: data,
                searchTitle: 'Resaults for "' + data + '"'
            });
        });
    };

    onMovieClick(id) {
        browserHistory.push(`/movie/${id}`)
    };

    render() {

        return <div className="query-page">
            <div className={"query-page--hero "+(this.state.lastQuery.length>0 ? 'query-page--hero--pull-up' : '')}
                 style={{backgroundImage: 'url('+this.state.bgImage+')'}}>
                <img className="query-page--hero--logo" src={Logo} alt="TheSearchMovieApp"/>
            </div>

            <div className="container ">
                <SearchField last={this.state.lastQuery} onQuery={(data)=>this.onSearchChanged(data)}></SearchField>
                <MovieList title={this.state.searchTitle} movieclick={(id)=>{this.onMovieClick(id)}}
                           list={this.state.queryList}></MovieList>
            </div>
        </div>
    };
}

export default Query;