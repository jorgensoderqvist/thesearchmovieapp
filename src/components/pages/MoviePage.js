import React, {Component} from 'react';
import API from '../../utils/API';
import {IMAGE} from '../../config/Constants'
import {browserHistory} from 'react-router';
import {LoadingManager} from '../loader/Loader'

import bg from '../../images/bg-movie2.jpg';
import  RankStar from './../rank-star/RankStar';

import  './Page.scss';
import  './MoviePage.scss';

class Movie extends Component {

    constructor() {
        super();
        this.state = {actors: [], movieObj: {}};
    };

    componentWillMount() {
        const movieId = this.props.params.movieId;
        if (movieId) {
            API.getMovieDetail(movieId).then((data)=> {
                this.setState({
                    movieObj: data
                });
                LoadingManager.hide();
            });
            API.getMovieActors(movieId).then((data)=> {
                this.setState({
                    actors: data.slice(0, 10)
                });
                LoadingManager.hide();
            });
        }
    };

    onActorClick(id) {
        browserHistory.push(`/actor/${id}`);
    };

    render() {

        const movie = this.state.movieObj;
        const actors = this.state.actors.map((actor)=> {
            return <li className="movie-page--actors--actor" key={actor.id}
                       onClick={()=>{this.onActorClick(actor.id)}}>{actor.name} </li>
        });

        const bgImage = (movie.backdrop_path) ? IMAGE.base_url + IMAGE.backdrop_sizes[2] + movie.backdrop_path : bg;
        const profileIMage  = (movie.poster_path) ? IMAGE.base_url+IMAGE.still_sizes[2]+movie.poster_path : null;

        return <div className="page movie-page">
            <div className="page--container" style={{backgroundImage: 'url('+bgImage+')'}}>
                <div className="page--background"></div>
                <div className="page--poster">
                    <img src={profileIMage} alt={movie.title}/>
                </div>
                <div className="page--info">
                    <h4>{movie.tagline}</h4>
                    <h1>{movie.title}</h1>
                    <RankStar rank={movie.vote_average}/>
                    <div
                        className="movie-page--year">{(movie.release_date) ? movie.release_date.substr(0, 4) : ''}</div>
                    <p>{movie.overview}</p>
                    <h4>Stars:</h4>
                    <ul className="movie-page--actors">
                        {actors}
                    </ul>
                </div>
            </div>
        </div>;
    };
}

export default Movie;