import React from 'react';
import ReactDOM from 'react-dom';
import {shallow, mount, render} from 'enzyme';
import MoviePage from './MoviePage';

import axios from 'axios';
import MockAdapter from 'axios-mock-adapter'

let wrapper;

const simpleProps = {
    params: {movieId: 123}
};
const mock = new MockAdapter(axios);


mock.onGet(/.movie\/123\?./).reply(200, {
    "adult": false,
    "backdrop_path": "/fCayJrkfRaCRCTh8GqN30f8oyQF.jpg",
    "belongs_to_collection": null,
    "budget": 63000000,
    "genres": [
        {
            "id": 18,
            "name": "Drama"
        }
    ],
    "homepage": "",
    "id": 550,
    "imdb_id": "tt0137523",
    "original_language": "en",
    "original_title": "Fight Club",
    "overview": "A ticking-time-bomb insomniac and a slippery soap salesman channel primal male aggression into a shocking new form of therapy. Their concept catches on, with underground \"fight clubs\" forming in every town, until an eccentric gets in the way and ignites an out-of-control spiral toward oblivion.",
    "popularity": 0.5,
    "poster_path": null,
    "production_companies": [
        {
            "name": "20th Century Fox",
            "id": 25
        }
    ],
    "production_countries": [
        {
            "iso_3166_1": "US",
            "name": "United States of America"
        }
    ],
    "release_date": "1999-10-12",
    "revenue": 100853753,
    "runtime": 139,
    "spoken_languages": [
        {
            "iso_639_1": "en",
            "name": "English"
        }
    ],
    "status": "Released",
    "tagline": "How much can you know about yourself if you've never been in a fight?",
    "title": "Fight Club",
    "video": false,
    "vote_average": 7.8,
    "vote_count": 3439
});


mock.onGet(/.movie\/123\/credits\?api_key./).reply(200, {
        "cast": [
            {
                "cast_id": 4,
                "character": "The Narratorr",
                "credit_id": "52fe4250c3a36847f80149f3",
                "id": 819,
                "name": "Edward Norton",
                "order": 0,
                "profile_path": null
            }
        ]
    }
);

beforeAll((done) => {
    wrapper = mount(<MoviePage {...simpleProps}></MoviePage>);
    setTimeout(() => {
        wrapper.update();
        done();
    }, 10);
});
afterAll((done) => {
    mock.reset();
});

it('should have movie movie title', ()=> {
    expect(wrapper.find('h1').text()).toBe('Fight Club');
});

it('should have one actor in the list', ()=> {
    expect(wrapper.find('.movie-page--actors--actor').length).toBe(1);
});

