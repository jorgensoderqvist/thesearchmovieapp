/**
 * Created by jorgensoderqvist on 2017-03-04.
 */
import React, {Component} from 'react';
import {browserHistory} from 'react-router'
import API from '../../utils/API'
import {IMAGE} from '../../config/Constants';
import {LoadingManager} from '../loader/Loader'

import bg from '../../images/bg_mov.jpg';
import MovieList from '../movie-list/MovieList';
import './Page.scss';
import './ActorPage.scss';

class Actor extends Component {

    constructor() {
        super();
        this.state = {movies: [], actorObj:{} };
    };

    componentWillMount() {
        const actorId = this.props.params.actorId;
        if (actorId) {
            API.getActor(actorId).then((data)=> {
                this.setState({
                    actorObj: data
                });
                LoadingManager.hide();
            });

            API.getMoviesByActor(actorId).then((data)=> {
                this.setState({
                    movies: data.slice(0,10)
                });
                LoadingManager.hide();
            });
        }
    };
    
    onMovieClick(id) {
        browserHistory.push(`/movie/${id}`);
    };

    getAge(actor){
        const a = new Date(actor.birthday);
        const to = (!actor.deathday) ? new Date() : new Date(actor.deathday);
        return to.getFullYear() - a.getFullYear();
    };

    render() {

        const actor = this.state.actorObj;
        const profileIMage  = (actor.profile_path) ? IMAGE.base_url+IMAGE.profile_sizes[2]+actor.profile_path : null;
        const age  = (actor.birthday) ? <h4 className="page--info--age">Age of {this.getAge(actor)}</h4> :null;
        const born = (actor.place_of_birth) ? <h4 className="page--info--born">Born in {actor.place_of_birth}</h4> : null;

        return <div className="page actor-page" >
            <div className="page--container" style={{backgroundImage: 'url('+bg+')'}}>
                <div className="page--background"></div>
                <div className="page--poster">
                    <img src={profileIMage} alt={actor.name}/>
                </div>
                <div className="page--info">
                    <h1>{actor.name}</h1>
                    {age}
                    {born}
                    <p>{actor.biography}</p>
                </div>
            </div>
            <div className="container">
                <h2>Known For</h2>
                <MovieList list={this.state.movies} movieclick={(id)=>this.onMovieClick(id)}></MovieList>
            </div>
        </div>;
    };
}
export  default Actor;