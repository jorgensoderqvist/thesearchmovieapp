import React, {Component} from 'react';
import { browserHistory } from 'react-router';

import './Loader.scss';

let timeoutId;

export const LoadingManager = {

    show() {
        if (this.loader) {
            this.loader.show();
        }
    },

    hide() {
        if (this.loader) {
            this.loader.hide();
        }
    },

    addLoader(obj) {
        this.loader = obj;
    }

}


class Loader extends Component {

    constructor() {
        super();
        LoadingManager.addLoader(this);
        this.state = {className: 'show'};
        
        browserHistory.listen( location =>  {
           this.show();
        });
    };

    show() {
        this.state = {className: 'show'};
    };

    hide() {
        this.setState({className: 'hide'});
        if(timeoutId){
            clearTimeout(timeoutId);
        }
        timeoutId= setTimeout(()=> {
            this.setState({className: 'hidden'});
        }, 1000);
    };


    render() {
        return <div className={"loader " + this.state.className }>
            <svg width='100%' height='100%' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"
                 preserveAspectRatio="xMidYMid" className="loader--ring">
                <rect x="0" y="0" width="100" height="100" fill="none" className="loader--ring--bk"></rect>
                <circle cx="50" cy="50" r="40" stroke="rgba(255,255,255,0.2)" fill="none" strokeWidth="10"
                        strokeLinecap="round"></circle>
                <circle cx="50" cy="50" r="40" stroke="rgba(255,255,255,0.8)" fill="none" strokeWidth="6"
                        strokeLinecap="round">
                    <animate attributeName="stroke-dashoffset" dur="2s" repeatCount="indefinite" from="0"
                             to="502"></animate>
                    <animate attributeName="stroke-dasharray" dur="2s" repeatCount="indefinite"
                             values="150.6 100.4;1 250;150.6 100.4"></animate>
                </circle>
            </svg>
        </div>
    }

}

export default Loader;
