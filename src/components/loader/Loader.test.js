import React from 'react';
import ReactDOM from 'react-dom';
import {shallow} from 'enzyme';
import Loader, {LoadingManager} from './Loader';


const wrapper = shallow(<Loader></Loader>);

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Loader></Loader>, div);
});

it('should show the loading class', () => {
    LoadingManager.show();
    expect(wrapper.find('.loader').hasClass('show'));
});

it('should hide the loading class', () => {
    LoadingManager.hide();
    expect(wrapper.find('.loader').hasClass('hide'));
});