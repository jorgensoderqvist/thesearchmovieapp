import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import SearchField from './SearchField';

let output;
let basicProps = {
    last:'latest text',
    onQuery:(a)=>{
        output= a;
    }
}

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<SearchField></SearchField>, div);
});

it('input value should be tha same as @last prop', () => {
    const wrapper = shallow(<SearchField {...basicProps} />);

    expect(wrapper.find('input').props().value).toEqual(basicProps.last);
});

it('Output sholud be same as input', () => {
    const wrapper = shallow(<SearchField {...basicProps}/>);
    wrapper.update();
    wrapper.find('input').simulate('change', {keyCode:13, target:{value:basicProps.last}});

    expect(output).toEqual(basicProps.last);
});

it('Output should not update if input is the same', () => {
    const wrapper = shallow(<SearchField {...basicProps}/>);
    wrapper.update();
    wrapper.find('input').simulate('change', {keyCode:13, target:{value:basicProps.last}});
    output= 'test2'
    wrapper.update();
    wrapper.find('input').simulate('change', {keyCode:13, target:{value:basicProps.last}});

    expect(output).toEqual('test2');
});

it('Output should not update if input is empty', () => {
    const wrapper = shallow(<SearchField {...basicProps}/>);
    output='hasNotfired';
    wrapper.update();
    wrapper.find('input').simulate('change', {keyCode:13, target:{value:''}});

    expect(output).toEqual('hasNotfired');
});


