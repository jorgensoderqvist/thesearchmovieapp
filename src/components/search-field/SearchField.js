/**
 * Created by jorgensoderqvist on 2017-03-04.
 */
import React, {Component} from 'react';
import './SearchField.scss';

let timerId;
let oldValue;

class SearchField extends Component {

    componentWillMount() {
        this.setState({
            value: this.props.last || ''
        });
    };

    onQuery(event) {
        this.setState({
            value: event.target.value
        });

        if (timerId) {
            clearTimeout(timerId);
        }

        if (event.keyCode === 13) {
            this.sendQuery();
            return;
        }

        timerId = setTimeout(()=> {
            this.sendQuery();
        }, 300);
    };

    sendQuery() {
        if (this.state.value === oldValue) {
            return;
        }
        if (this.state.value.length === 0) {
            return;
        }

        this.props.onQuery(this.state.value);
        oldValue = this.state.value;
    };

    render() {
        return <div><input className="search-field" placeholder="Search a movie" value={this.state.value}
                           ref={(input) => { this.queryInput = input; }} onChange={(e)=>this.onQuery(e)}/></div>
    };
}

export default SearchField;