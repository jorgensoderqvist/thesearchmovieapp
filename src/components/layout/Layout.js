/**
 * Created by jorgensoderqvist on 2017-03-04.
 */
import React from 'react';
import './Layout.scss';
import '../../fonts/fonts.scss'

import Loader from '../loader/Loader';

export default class Layout extends React.Component {

    render(){
        return (
            <div>
                <Loader />
                {this.props.children}
            </div>
        )
    }
}