import React from 'react';
import ReactDOM from 'react-dom';
import {shallow} from 'enzyme';
import MovieList from './MovieList';

let output;
const simpleProps = {
    title: 'Lorum ipsum',
    movieclick: (id)=> {
        output = id;
    },
    list: [
        {
            adult: false,
            backdrop_path: "/rXBB8F6XpHAwci2dihBCcixIHrK.jpg",
            genre_ids: Array[2],
            id: 341174,
            original_language: "en",
            original_title: "Fifty Shades Darker",
            overview: "When a wounded Christian Grey tries to entice a cautious Ana Steele back into his life, she demands a new arrangement before she will give him another chance. As the two begin to build trust and find stability, shadowy figures from Christian’s past start to circle the couple, determined to destroy their hopes for a future together.",
            popularity: 178.3487,
            poster_path: "/aybgjbFbn6yUbsgUMnUbwc2jcWd.jpg",
            release_date: "2017-02-08",
            title: "Fifty Shades Darker",
            video: false,
            vote_average: 6,
            vote_count: 1015
        },
        {
            adult: false,
            backdrop_path: "/5pAGnkFYSsFJ99ZxDIYnhQbQFXs.jpg",
            genre_ids: Array[3],
            id: 263115,
            original_language: "en",
            original_title: "Logan",
            overview: "In the near future, a weary Logan cares for an ailing Professor X in a hide out on the Mexican border. But Logan's attempts to hide from the world and his legacy are up-ended when a young mutant arrives, being pursued by dark forces.",
            popularity: 151.138324,
            poster_path: "/45Y1G5FEgttPAwjTYic6czC9xCn.jpg",
            release_date: "2017-02-28",
            title: "Logan",
            video: false,
            vote_average: 7.8,
            vote_count: 938,
        }
    ]
}


it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<MovieList {...simpleProps}></MovieList>, div);
});

it('Passes the title as props', () => {
    const wrapper = shallow(<MovieList {...simpleProps}></MovieList>);
    expect(wrapper.find('h2').html()).toEqual(
        `<h2 class="movie-list--headline">Lorum ipsum</h2>`
    );
});

it('has 2 items in list', () => {
    const wrapper = shallow(<MovieList {...simpleProps}></MovieList>);
    expect(wrapper.find('li').length).toEqual(2);
});

it('is the right id when clicked', () => {
    const wrapper = shallow(<MovieList {...simpleProps}></MovieList>);
    wrapper.find('li').at(1).simulate('click');
    expect(output).toEqual(simpleProps.list[1].id);
});
