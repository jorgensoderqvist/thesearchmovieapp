/**
 * Created by jorgensoderqvist on 2017-03-04.
 */
import React, {Component} from 'react';
import {IMAGE} from '../../config/Constants'
import image from '../../images/no-image.png';
import RateStar from '../rank-star/RankStar';

import './MovieList.scss';

class SearchList extends Component {


    onItemClick(id) {
        this.props.movieclick(id);
    }

    render() {

        const items = this.props.list.map((item)=> {
            const releaseDate = (item.release_date) ? <p>{item.release_date.substr(0, 4)}</p> : '';
            return <li className="movie-list--list--item" key={item.id+(Math.random()*100).toFixed()}
                       onClick={()=>{this.onItemClick(item.id)}}
                       style={{backgroundImage: 'url('+(item.poster_path ? IMAGE.base_url+IMAGE.still_sizes[1] + item.poster_path: image)+')'}}>
                <div className="movie-list--list--item--info">
                    <div className="movie-list--list--item--info--holder">
                        <p>{item.title}</p>
                        {releaseDate}
                        <RateStar rank={item.vote_average}></RateStar>
                    </div>
                </div>
            </li>
        });

        return <div className="movie-list">
            <h2 className="movie-list--headline">{this.props.title}</h2>
            <ul className="movie-list--list">
                {items}
            </ul>
        </div>
    }
}

export default SearchList;